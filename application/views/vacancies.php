<?php
//$query = $this->db->get("vacancies");
$query = $this->db->get_where('vacancies', array('vCat' => 'Post Doctoral'));
$postdoc = $query->result();

$query = $this->db->get_where('vacancies', array('vCat' => 'PHD'));
$phd = $query->result();

$query = $this->db->get_where('vacancies', array('vCat' => 'Research'));
$research = $query->result();

$query = $this->db->get_where('vacancies', array('vCat' => 'General'));
$general = $query->result();
?> 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
    	<h1>Vacancies</h1>
    	
    	<h3>Phd Positions</h3>
    	<!-- phd -->
    	<div id="phd">
			<?php
	    	$count = 1;
	    	
	    	foreach($phd as $o)
			{
				if($count % 2 != 0)
				{
					echo '
						<!-- row -->
		    			<div class="row">
					';
				}
				
				echo '
				
					<div class="col-xs-6">
		    		  <!-- general form elements -->
			          <div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title">'.$o->title.'</h3>
			              <span style="float: right"><a href="<?php echo base_url();?>index.php/applyJob">Apply Now!</a></span>
			            </div>
				        <div class="table-responsive">
				          <table class="table">
				            <tr>
				              <th>Apply By: <span style="color: red"> '.$o->deadline.' </span></th>
				            </tr>
				            <tr>
				              <td>'.$o->description.'</td>
				            </tr>
				          </table>
				        </div>
			          </div>
			          <!-- /.box -->
					</div>
				
				';
				
				if($count % 2 == 0)
				{
					echo '
						<!-- row -->
		    			</div>
					';
				}
				
				$count++;
			}
	    	
	    	?>	
    	</div>
    	<!-- /.phd -->
    	
    	<h3>Post Doctoral Positions</h3>
    	<!-- post doc -->
    	<div id="postDoc">
	    	<?php
	    	$count = 1;
	    	
	    	foreach($postdoc as $o)
			{
				if($count % 2 != 0)
				{
					echo '
						<!-- row -->
		    			<div class="row">
					';
				}
				
				echo '
				
					<div class="col-xs-6">
		    		  <!-- general form elements -->
			          <div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title">'.$o->title.'</h3>
			              <span style="float: right"><a href="<?php echo base_url();?>index.php/applyJob">Apply Now!</a></span>
			            </div>
				        <div class="table-responsive">
				          <table class="table">
				            <tr>
				              <th>Apply By: <span style="color: red"> '.$o->deadline.' </span></th>
				            </tr>
				            <tr>
				              <td>'.$o->description.'</td>
				            </tr>
				          </table>
				        </div>
			          </div>
			          <!-- /.box -->
					</div>
				
				';
				
				if($count % 2 == 0)
				{
					echo '
						<!-- row -->
		    			</div>
					';
				}
				
				$count++;
			}
	    	
	    	?>
    	</div>
    	<!-- /.post doc -->
    	
    	<h3>Research Positions</h3>
    	<!-- research -->
    	<div id="research">
	    	<?php
	    	$count = 1;
	    	
	    	foreach($research as $o)
			{
				if($count % 2 != 0)
				{
					echo '
						<!-- row -->
		    			<div class="row">
					';
				}
				
				echo '
				
					<div class="col-xs-6">
		    		  <!-- general form elements -->
			          <div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title">'.$o->title.'</h3>
			              <span style="float: right"><a href="<?php echo base_url();?>index.php/applyJob">Apply Now!</a></span>
			            </div>
				        <div class="table-responsive">
				          <table class="table">
				            <tr>
				              <th>Apply By: <span style="color: red"> '.$o->deadline.' </span></th>
				            </tr>
				            <tr>
				              <td>'.$o->description.'</td>
				            </tr>
				          </table>
				        </div>
			          </div>
			          <!-- /.box -->
					</div>
				
				';
				
				if($count % 2 == 0)
				{
					echo '
						<!-- row -->
		    			</div>
					';
				}
				
				$count++;
			}
	    	
	    	?>
    	</div>
    	<!-- /.research -->
    	
    	<h3>General Positions</h3>
    	<!-- general -->
    	<div id="general">
	    	<?php
	    	$count = 1;
	    	
	    	foreach($general as $o)
			{
				if($count % 2 != 0)
				{
					echo '
						<!-- row -->
		    			<div class="row">
					';
				}
				
				echo '
				
					<div class="col-xs-6">
		    		  <!-- general form elements -->
			          <div class="box box-primary">
			            <div class="box-header with-border">
			              <h3 class="box-title">'.$o->title.'</h3>
			              <span style="float: right"><a href="<?php echo base_url();?>index.php/applyJob">Apply Now!</a></span>
			            </div>
				        <div class="table-responsive">
				          <table class="table">
				            <tr>
				              <th>Apply By: <span style="color: red"> '.$o->deadline.' </span></th>
				            </tr>
				            <tr>
				              <td>'.$o->description.'</td>
				            </tr>
				          </table>
				        </div>
			          </div>
			          <!-- /.box -->
					</div>
				
				';
				
				if($count % 2 == 0)
				{
					echo '
						<!-- row -->
		    			</div>
					';
				}
				
				$count++;
			}
	    	
	    	?>
    	</div>
    	<!-- /.general -->
    </section>
   
    
  </div>
  <!-- /.content-wrapper -->
  
<!-- includes footer -->

<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>plugins/select2/select2.full.min.js"></script>\
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
  });
</script>
