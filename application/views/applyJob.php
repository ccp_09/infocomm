

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
    	<div class="row">
    		<div class="col-xs-6">
    		  <!-- general form elements -->
	          <div class="box box-primary">
	            <div class="box-header with-border">
	              <h3 class="box-title">Apply</h3>
	            </div>
	            <!-- /.box-header -->
	            <!-- form start -->
	            <form role="form">
	              <div class="box-body">
	                
	                <div class="form-group">
	                  <input type="text" class="form-control" placeholder="Name">
	                </div>
	                
	                <div class="form-group">
	                  <div class="input-group">
	                    <div class="input-group-addon">
	                      <i class="fa fa-envelope"></i>
	                    </div>
	                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
	                  </div>
	                </div>
	                
	                <div class="form-group">
	                  <div class="input-group">
	                    <div class="input-group-addon">
	                      <i class="fa fa-phone"></i>
	                    </div>
	                    <input type="tel" class="form-control" placeholder="Contact">
	                  </div>
	                </div>
	                
	                <div class="form-group">
	                  <input type="url" class="form-control" placeholder="Website">
	                </div>
	                
	                <div class="form-group">
	                  <label>Country</label>
	                  <select class="form-control">
	                    <option disabled="disabled" selected="selected">Select</option>
	                    <option>Country 1</option>
	                    <option>Country 2</option>
	                    <option>Country 3</option>
	                  </select>
	                </div> 
	                
	                <div class="form-group">
	                  <label for="exampleInputFile">Upload CV</label>
	                  <input type="file" id="exampleInputFile">
	                </div>
	                
	                <label>References</label>
		            
		            <div class="form-group">
	                  <input type="text" class="form-control" placeholder="Name">
	                </div>
	                
	                <div class="form-group">
	                  <input type="text" class="form-control" placeholder="Position">
	                </div>
		            
	              	<div class="form-group">
	                  <div class="input-group">
	                    <div class="input-group-addon">
	                      <i class="fa fa-phone"></i>
	                    </div>
	                    <input type="tel" class="form-control" placeholder="Contact">
	                  </div>
	                </div>
	                
	                <p><a href="#">+ Add Another Reference</a></p>
	                <!-- /.box-body -->
	                
	                <br />
	                
	                <!-- checkbox -->
	                <div class="form-group">
	                  <div class="checkbox">
	                    <label>
	                      <input type="checkbox" id="loginRequired">
	                      I consent that the information provided here is correct and honest.
	                    </label>
	                  </div>
	                </div>
	
	                <div class="box-footer">
	                  <button type="submit" class="btn btn-primary">Add</button>
	                </div>
	              </div>
	            </form>
	          </div>
	          <!-- /.box -->
    		</div>
    		<div class="col-xs-6">
		           
	           <div class="row">
	           	  <h2>Job Details</h2>
	    			<p class="lead">Research Assistantship</p>

			        <div class="table-responsive">
			          <table class="table">
			            <tr>
			              <th style="width:50%">Deadline:</th>
			              <td>3/3/17</td>
			            </tr>
			            <tr>
			              <th>Date Posted:</th>
			              <td>3/3/16</td>
			            </tr>
			            <tr>
			              <th>Type:</th>
			              <td>Post Doctoral</td>
			            </tr>
			            <tr>
			              <th>Job Description:</th>
			              <td>
			          		<p>- Showing up to work at 7 am</p>
			          		<p>- Researching</p>
			          		<p>- Handling Research</p>
						  </td>
			            </tr>
			          </table>
			        </div>
	    	   </div>
	    	   <div class="row">
	    	   	<h2>Similar Posting</h2>
	    			<p class="lead">
	    				Research Assistantship
	    				<span style="float: right"><a href="<?php echo base_url();?>index.php/applyJob">Apply Now!</a></span>
    				</p>

			        <div class="table-responsive">
			          <table class="table">
			            <tr>
			              <th style="width:50%">Deadline:</th>
			              <td>3/3/17</td>
			            </tr>
			            <tr>
			              <th>Date Posted:</th>
			              <td>3/3/16</td>
			            </tr>
			            <tr>
			              <th>Type:</th>
			              <td>Post Doctoral</td>
			            </tr>
			            <tr>
			              <th>Job Description:</th>
			              <td>
			          		<p>- Showing up to work at 7 am</p>
			          		<p>- Researching</p>
			          		<p>- Handling Research</p>
						  </td>
			            </tr>
			          </table>
			        </div>

	    	   </div>
    		</div>
    	</div>
    </section>
   
    
  </div>
  <!-- /.content-wrapper -->
  
<!-- includes footer -->

<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>plugins/select2/select2.full.min.js"></script>\
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
  });
</script>
