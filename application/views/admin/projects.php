
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Projects
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
	                <tr>
	                  <th>Name</th>
	                  <th>Owner</th>
	                  <th>Category</th>
	                  <th>Start Date</th>
	                  <th>End Date</th>
	                  <th>Approval Status</th>
	                  <th>Delete</th>
	                </tr>
                </thead>
                <tbody>
	                <tr>
	                  <td>Mission to Mars</td>
	                  <td>Captain Marvel</td>
	                  <td>Space Missions</td>
	                  <td>3-2-1995</td>
	                  <td>5-6-2017</td>
	                  <td><span class="label label-success">Approved</span></td>
	                  <td>delete</td>
	                </tr>
	                <tr>
	                  <td>Making Sentient Nanobots</td>
	                  <td>Lena Luthor</td>
	                  <td>Nanotechnology</td>
	                  <td>3-2-1795</td>
	                  <td>5-6-2117</td>
	                  <td><span class="label label-warning">Pending</span></td>
	                  <td>delete</td>
	                </tr>
                <!--  
                	<td><span class="label label-warning">Pending</span></td>
                  	<td><span class="label label-primary">Activated</span></td>
                  	<td><span class="label bg-gray" style="color:#FFF">Deactivated</span></td>
                  	<td><span class="label label-info">Suspended</span></td>
                  -->
                </tbody>
                <tfoot>
	                <tr>
	                  <th>Name</th>
	                  <th>Owner</th>
	                  <th>Category</th>
	                  <th>Start Date</th>
	                  <th>End Date</th>
	                  <th>Approval Status</th>
	                  <th>Delete</th>
	                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
<!-- includes footer -->

<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

