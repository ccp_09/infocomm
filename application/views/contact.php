

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
    	
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Please fill in the form to contact us</h3>
              <h6>If you have any questions or queries simply leave us a message and we will get back to you at our earliest. Thank you.</h6>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Name">
                </div>
                
                <div class="form-group">
	                  <div class="input-group">
	                    <div class="input-group-addon">
	                      <i class="fa fa-envelope"></i>
	                    </div>
	                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
	                  </div>
	                </div>
	                
	                <div class="form-group">
	                  <div class="input-group">
	                    <div class="input-group-addon">
	                      <i class="fa fa-phone"></i>
	                    </div>
	                    <input type="tel" class="form-control" placeholder="Contact">
	                  </div>
	                </div>
                
              	<div class="form-group">
                  <textarea class="form-control" rows="5" placeholder="Your message here ..."></textarea>
                </div>
                                                
              </div>
              
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="reset" class="btn btn-success">Clear</button>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->

        <!-- right column -->
        <div class="col-md-6">

        <div id="googleMap" style="width:100%;height:400px;"></div>

        <script>
          function myMap() {
            var mapProp= {
            center:new google.maps.LatLng(56.4656735,84.947323),
            zoom:15,
          };
          var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
          }
          </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0mbpOLT5Y6HXMjcKifs_1cCge-Ik3wT8&callback=myMap"></script>

        </div>


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    
  </div>
  <!-- /.content-wrapper -->
  
<!-- includes footer -->

<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>plugins/select2/select2.full.min.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
