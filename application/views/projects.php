
  <!-- Full Width Column -->
  

      <!-- Main content -->
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Projects
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
                
        <?php
		
			$query = $this->db->get('user_projects');
			$output = $query->result();
			//$output = $query->row();			
			foreach ($output as $o)
			{
				echo '
				<div class="col-xs-6">
				<div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">'.$o->projectTitle.'</h3>
        
                </div>
                <div class="box-body">
                    <div class="project_thumbmail"><img src="'.base_url().'dist/img/user4-128x128.jpg" class="img img-responsive" /></div>
                    <div class="project_container">
                    	<div class="project_category_date"><span>Fix Categories</span><span><br/>'.$o->dateCreated.'</span></div>
                    	<div class="project_content">'.$o->description.'
</div>
                    </div>
              </div>
              <div class="box-footer">
                  <p align="right">Read More.....</p>
                </div>
                </div>
				</div>';
			}
		?>
                
                
                
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
     
  <!-- /.content-wrapper -->
  