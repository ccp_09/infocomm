  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
            <div class="row">
			
            <div align="center">
            
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="<?php echo base_url() ?>dist/img/eventimages/eventheadlinebanner.png" alt="First slide">

                    <div class="carousel-caption">
                      First Slide
                    </div>
                  </div>
                  <div class="item">
                    <img src="<?php echo base_url() ?>dist/img/eventimages/eventheadlinebanner.png" alt="Second slide">

                    <div class="carousel-caption">
                      Second Slide
                    </div>
                  </div>
                  <div class="item">
                    <img src="<?php echo base_url() ?>dist/img/eventimages/eventheadlinebanner.png" alt="Third slide">

                    <div class="carousel-caption">
                      Third Slide
                    </div>
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
            
			</div>          

          </div>
          <!-- end of row -->
          
          <br />
     
     <div class="row">
     
     <div class="col-md-12">
     
     <div class="box box-primary">
     <div class="box-header with-border">
     <h3 class="box-title">Upcoming Events</h3>     
     <div class="box-body">
     
     <table class="table table-hover">
            
            <tbody>
            
            <tr>
              <td colspan="2"><strong>Event Name</strong></td>

              <td colspan="2"><strong>Event Name</strong></td>

            </tr>
            
            <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/eventimages/event.png" width="300" height="300">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>
             
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/eventimages/event.png" width="300" height="300">
              </p>
              </td> 
                        
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>            
            </tr>
            
            <tr>
              <td colspan="2"><strong>Event Name</strong></td>

              <td colspan="2"><strong>Event Name</strong></td>

            </tr>
            
            <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/eventimages/event.png" width="300" height="300">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>
             
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/eventimages/event.png" width="300" height="300">
              </p>
              </td> 
                        
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>            
            </tr>
            <tr>
            <td colspan="4">
                  <ul class="pagination pagination-sm no-margin pull-right">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
            </td>
            </tr>
            </tbody>
            </table>
 
     </div>
     </div>
     </div>
     
     </div>
     
     </div>
        
      
           <div class="row">
     
     <div class="col-md-12">
     
     <div class="box box-primary">
     <div class="box-header with-border">
     <h3 class="box-title">Past Events</h3>     
     <div class="box-body">
     
     <table class="table table-hover">
            
            <tbody>
            
            <tr>
              <td colspan="2"><strong>Event Name</strong></td>

              <td colspan="2"><strong>Event Name</strong></td>

            </tr>
            
            <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/eventimages/event.png" width="300" height="300">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>
             
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/eventimages/event.png" width="300" height="300">
              </p>
              </td> 
                        
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>            
            </tr>
            
            <tr>
              <td colspan="2"><strong>Event Name</strong></td>

              <td colspan="2"><strong>Event Name</strong></td>

            </tr>
            
            <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/eventimages/event.png" width="300" height="300">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>
             
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/eventimages/event.png" width="300" height="300">
              </p>
              </td> 
                        
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>            
            </tr>
            <tr>
            <td colspan="4">
                            <ul class="pagination pagination-sm no-margin pull-right">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
            </td>
            </tr>
            </tbody>
            </table>
 
     </div>
     </div>
     </div>
     
     </div>
     
     </div>  
        
        
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->