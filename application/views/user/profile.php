<?php
	if($this->session->userdata('email') == ''){
		header("location: http://localhost/infocomm/user/login");
	}else{
		$email = $this->session->userdata('email');
		
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('email', $email);
		$this->db->where('status', 'active');  
		
		$query = $this->db->get();
		$output = $query->result();
		foreach($output as $result){
			$id = $result->userID;
			$titlee = $result->title;
			$fname = $result->fname;
			$lname = $result->lname;
			$designation = $result->designation;
			$city = $result->city;
			$country = $result->country;
			$phone = $result->phone;
			$bio = $result->bio;
			$interests = $result->interest;
			$facebook = $result->facebook;
			$googleplus = $result->googlePlus;
			$linkedIn = $result->linkedIn;
			$twitter = $result->twitter;
			$website = $result->website;
		}
		
		$interestArray = explode(";", $interests);
		
		$this->db->select('*');
		$this->db->from('user_education');
		$this->db->where('userID', $id);
		$query = $this->db->get();
		$output = $query->result();
		
		$eduOutput = '';
		foreach($output as $userEducation){
			$institue = $userEducation->institute;
			$qualification = $userEducation->qualification;
			
			$eduOutput .= $qualification.", ".$institue."<br/>";
		}
	}
?>
  <!-- Full Width Column -->  

      <!-- Main content -->
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url() ?>resources/profile_image/user3-128x128.jpg" alt="User profile picture" width="300" height="300">

              <h3 class="profile-username text-center"><?php echo $titlee.' '.$fname.' '.$lname; ?></h3>

              <p class="text-muted text-center"><?php echo $designation ?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right">125</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">15</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">150</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div id="aboutUS" class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3><button id="editProfile" type="button" style="float:right;" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Edit Profile</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education </strong>

              <p class="text-muted">
                <?php echo $eduOutput; ?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted"><?php echo $city.", ".$country ?></p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Interests</strong>

              <p>
              <?php
			  	  $interestColor = array('success','danger','info','warning','primary'); 
				  foreach($interestArray as $interest){
					  $color = array_shift($interestColor);
					  echo '<span class="label label-'.$color.'">'.$interest.'</span> ';
					  array_push($interestColor, $color);
				  }
                /*<span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>*/
				?>
              </p>

              <hr>

             <!-- <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>I hope I could be the best in CCP this semester</p>-->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
              <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
              <li><a href="#projects" data-toggle="tab">View Project</a></li>
              <li><a href="#publication" data-toggle="tab">View Publication</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                	<h4 id="something"> You have no Activity yet</h4>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
					<h4> You have nothing on your timeline yet</h4>
              </div>
              <!-- /.tab-pane -->
			  <!----/.Create projects--->
              <div class="tab-pane" id="projects">
                <!-- Default box -->
                <div align="right" style="margin-bottom:5px;"><a href="projects" class="btn btn-success">Add Project</a></div>
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Title</h3>
        
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                      <i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                    <div class="project_thumbmail"><img src="<?php echo base_url() ?>dist/img/user4-128x128.jpg" class="img img-responsive" /></div>
                    <div class="project_container">
                    	<div class="project_category_date"><span>Nano technology</span><span>&nbsp;&nbsp;&nbsp;25 September, 2017</span></div>
                    	<div class="project_content">The landed planes should wait for at least 60 minutes before take-off.
My software simulation however is incomplete because I haven’t implemented airplane transfer and inbound simulation for airplane to a destination airport and also I haven’t done the web GUI.
</div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <p align="right">Read More.....</p>
                </div>
                <!-- /.box-footer-->
              </div>
              <!-------End of Box--------------------->
              </div>
              <!----/.End of Create projects--->
              <!----/.Create Publication--->
              <div class="tab-pane" id="publication">
                <div align="right" style="margin-bottom:5px;"><a href="publications" class="btn btn-success">Add Publication</a></div>
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Title</h3>
        
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                      <i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                    <div class="project_thumbmail"><img src="<?php echo base_url() ?>dist/img/user4-128x128.jpg" class="img img-responsive" /></div>
                    <div class="project_container">
                    	<div class="project_category_date"><span>Nano technology</span><span>&nbsp;&nbsp;&nbsp;25 September, 2017</span></div>
                    	<div class="project_content">The landed planes should wait for at least 60 minutes before take-off.
My software simulation however is incomplete because I haven’t implemented airplane transfer and inbound simulation for airplane to a destination airport and also I haven’t done the web GUI.
</div>
                    </div>
              </div>
              <div class="box-footer">
                  <p align="right">Read More.....</p>
                </div>
                </div>
              <!----/.End of Create Publication--->
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div data-id="<?php echo $id ?>" id="getUpdate" class="modal-content">
        
          	
         
      </div>
    </div>
  </div>