
  <!-- Full Width Column -->
  

      <!-- Main content -->
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        My Projects
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">

              <h3 class="profile-username text-center">Ashton Sally</h3>

              <p class="text-muted text-center">Software Engineer</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right">125</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">15</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">150</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
                B.S. in Software Engineering from Curtin University, Australia.
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Colombo, Sri Lanka</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>I hope I could be the best in CCP this semester</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-8">
        	<div class="box">
            	<div class="box-header with-border">
                	<h3 class="box-title">CREATE PROJECT</h3>
                </div>
                <div class="box-body">
            	<form class="form-horizontal">
                      <div class="form-group">
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="projTitle" name="" placeholder="Project Title" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="projMemb" name="" placeholder="Project Members" />
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="projCollab" name="" placeholder="Project Collaborators" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="projResArea" name="" placeholder="Project Research Area" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-1">Duration</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="projFrom" name="" placeholder="From" />
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="projTo" name="" placeholder="To" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="projFunding" name="" placeholder="Funding Companies" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">
                                <textarea class="form-control" id="projDescrip" placeholder="Project Description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="projPub" name="" placeholder="Publications" />
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="projStatus" name="" placeholder="Status" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">
                                    <input type="file" class="form-control" id="" name="" placeholder="Upload files" />
                            </div>
                        </div>
                      <div class="form-group">
                        <div class="col-sm-7">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                            </label>
                          </div>
                        </div>
                        <div class="">
                        <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                    </div>
          	</div>
      <!-- /.box -->
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
     
  <!-- /.content-wrapper -->
  