<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title;  ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url() ?>plugins/iCheck/square/blue.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() ?>plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="<?php echo base_url() ?>"><b>INFOCOMM</b></a>
  </div>

  <div class="register-box-body">
  <p class="login-box-msg">Register as a Member</p>
    <button class="btn btn-default" id="Infocomm" type="button">Infocomm Users</button>
    <button class="btn btn-default" id="External" type="button">External Users</button>
    <div id="formbody">
        <form action="" id="regForm">
        <!--------Form Body--->
        <!--////////////////////// Register-->
            <div class="form-group has-feedback">
                <label>Title</label>
                <select class="form-control" id="title">
                    <option></option>
                    <option>Dr.</option>
                    <option>Prof.</option>
                    <option>Mr.</option>
                    <option>Mrs.</option>
                    <option>Ms.</option>
                </select>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" id="fname" placeholder="First name" onKeyUp="textval(this)">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" id="lname" placeholder="Last name" onKeyUp="textval(this)">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <label>Gender</label>
                <select id="gender" class="form-control">
                    <option></option>
                    <option>Male</option>
                    <option>Female</option>
                </select>
                <span class="help-block"></span>
              </div>
              <div class="form-group">
                <div class="input-group date">
                <input type="text" class="form-control pull-right" id="DOB" placeholder="Date of Birth (DD/MM/YYY)">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <span id="errorDOB" class="help-block"></span>
                  
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group has-feedback">
                <input type="email" class="form-control" id="email" placeholder="Email">
                <span id="spanEmail" class="glyphicon glyphicon-envelope form-control-feedback"></span>
                <span id="errorEmail" class="help-block"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" id="password" placeholder="Password">
                <span id="spanPwd" class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span id="errorPwd" class="help-block"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" id="R_password" placeholder="Confirm password">
                <span id="spanRpwd" class="glyphicon glyphicon-lock form-control-feedback"></span>
                <span id="errorRpwd" class="help-block"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" id="city" placeholder="City">
                <span class="form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" id="phone" placeholder="Mobile Number">
                <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="text" class="form-control" id="country" placeholder="Country">
                <span class="glyphicon glyphicon-globe form-control-feedback"></span>
              </div>
              <div class="row">
                <div class="col-xs-8">
                  <div class="checkbox icheck">
                    <label>
                      <input id="t_c" type="checkbox"> I agree to the <a href="#">terms</a>
                    </label>
                  </div>
                
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                <input value="" type="hidden" id="type">
                  <button type="button" id="regBtn" data-id1="" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
              </div>
          
        </form>
        <!--------////////////////////////////////////////////////////////////////-->
    </div><!--------/. Form Body--->
	<br/>
    <a href="<?php echo base_url() ?>user/login" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url() ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url() ?>bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url() ?>plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url() ?>dist/js/action.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
	$('#datepicker').datepicker({
      autoclose: true
    });
  });
</script>
<script>

/////////////////////////////////////////////////////////////////////
/**
*** Validates numbers and text
**/
/////////////////////////////////////////////////////////////////////
function regval(e){//validate text
	var r = /[^a-z0-9]/gi;
	e.value = e.value.replace(r,"");
}
function textval(e){//validate text
	var r = /[^a-z]/gi;
	e.value = e.value.replace(r,"");
}

/////////////////////////////////////////////////////////////////////
/**
*** Validates email function
**/
/////////////////////////////////////////////////////////////////////
function validateEmail() {
	var expression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (expression.test($('#email').val())) {
		$('#spanEmail').css('color', '#0F9816');
		$('#errorEmail').html("");
		return true;
	}
	else {
		$('#spanEmail').css('color', 'inherit');
		$('#errorEmail').html("Invalid Email");
		return false;
	}
}

/////////////////////////////////////////////////////////////////////
/**
*** Validates password function
**/
/////////////////////////////////////////////////////////////////////
function validatePassword(){
	var pwd = $('#password').val();
	var checkstrength = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
	if(pwd == ''){
		$('#errorPwd').html("Password must not be empty");
	}else if(pwd.length < 7){
		$('#errorPwd').html("Password must be greater than 7 characters");
		$('#spanPwd').css('color', 'inherit');
		$('#spanPwd').removeClass('glyphicon-ok').addClass('glyphicon-lock');
		return false;	
	}else if(!pwd.match(checkstrength)){
		$('#errorPwd').html("password must contain at least one uppercase, lowercase and number");
		$('#spanPwd').css('color', 'inherit');
		$('#spanPwd').removeClass('glyphicon-ok').addClass('glyphicon-lock');	
		return false;
	}else{
		$('#spanPwd').css('color', '#0F9816');
		$('#spanPwd').removeClass('glyphicon-lock').addClass('glyphicon-ok');
		$('#errorPwd').html("");
		return true;
	}	
}

/////////////////////////////////////////////////////////////////////
/**
*** Retype and match password function
**/
/////////////////////////////////////////////////////////////////////
function matchPassword() {
	var pwd = $('#password').val();
	var rpwd = $('#R_password').val();
	if(rpwd.match(pwd)){
		$('#spanRpwd').css('color', '#0F9816');
		$('#spanRpwd').removeClass('glyphicon-lock').addClass('glyphicon-ok');
		$('#errorRpwd').html("");
		return true;
	}else{
		$('#spanRpwd').css('color', 'inherit');
		$('#spanRpwd').removeClass('glyphicon-ok').addClass('glyphicon-lock');
		$('#errorRpwd').html("Password don't match");
		return false;
	}
}

/////////////////////////////////////////////////////////////////////
/**
*** Validate date of birth
**/
/////////////////////////////////////////////////////////////////////
function dobValidate(){
	var DOB = $('#DOB').val();
	if($('DOB:contains(-)')){
		var day = DOB.split('-')[1];
		var Month = DOB.split('-')[2];
		if(day <= 31  && Month <= 12){
			$('#errorDOB').html('')
			return true;
		}else{
			$('#errorDOB').html('Invalid Date of Birth')
			return false;
		}
	}
}


$(document).ready(function() {
	$('.help-block').css('color', '#F70004');
	
	/////////////////////////////////////////////////////////////////////
	/**
	*** Check if user is a guest and load resources
	**/
	/////////////////////////////////////////////////////////////////////
	$('#External').on('click', function(){
		$('#External').hide()
		$('#Infocomm').hide() 
		$('.login-box-msg').html('Registering as an External user  ');
		$('.login-box-msg').append('<button class="btn btn-danger" id="reset" type="submit">Reset</button>');
		$('#regForm').find(':input').removeAttr('disabled');
		$('#type').val('external')
	})
	
	/////////////////////////////////////////////////////////////////////
	/**
	*** Check if user is an infocomm employee and load resources
	**/
	/////////////////////////////////////////////////////////////////////
	$('#Infocomm').on('click', function(){
		$('#regForm').find(':input').attr('disabled', 'disabled');
		var activationPrompt = prompt('Please enter activation code from admin', '');
		var code = 1234;
		if(activationPrompt == code){
			$('#External').hide()
			$('#Infocomm').hide()
			$('.login-box-msg').html('Registering as an Infocomm user  ');
			$('.login-box-msg').append('<button class="btn btn-danger" id="reset" type="submit">Reset</button>');
			$('#regForm').find(':input').removeAttr('disabled');
			$('#type').val('internal')
		}else{
		}
	})
	
	
	$(document).on('click','#reset', function(){
		$('#External').show()
		$('#Infocomm').show()
		$('#reset').hide()
		$('.login-box-msg').html('Registering as a member  ');
		$('#type').val('')
	})
	
	/////////////////////////////////////////////////////////////////////
	/**
	*** Validate password, email when a user moves from an input textbox
	**/
	/////////////////////////////////////////////////////////////////////
	$('#email').blur(validateEmail);
	$('#password').blur(validatePassword);
	$('#R_password').blur(matchPassword);
	$('#DOB').blur(dobValidate);
	$(document).on('click','#regBtn',function(){
		
		var title = $('#title option:selected').text();
		var fname = $('#fname').val();
		var lname = $('#lname').val();
		var gender = $('#gender option:selected').text();
		var DOB = $('#DOB').val();
		var email = $('#email').val();
		var pwd = $('#password').val();
		var city = $('#city').val();
		var phone = $('#phone').val();
		var country = $('#country').val();
		var userType = $('#type').val();
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Post to database
		**/
		/////////////////////////////////////////////////////////////////////
		if((title&&fname&&lname&&gender&&DOB&&email&&country&&phone) !== ''){
			if(userType != ''){
				if($('#t_c').is(':checked')){
					if(validateEmail() && validatePassword() && matchPassword() && dobValidate()){
						$.ajax({
							url:"register/adduser",
							type:"POST",
							data:{title:title,fname:fname,lname:lname,gender:gender,DOB:DOB,email:email,pwd:pwd,city:city,phone:phone,country:country,userType:userType},
							success: function(data){
								alert("Added");
								location.reload();
							}
						})
					}
				}else{
					alert('Terms and conditions must be checked');
				}
			}else{
				alert("Select a Member type");
			}
			
		}else{
			alert("Form must not be Empty")
		}
	})
});
</script>
</body>
</html>
