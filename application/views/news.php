

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
            <div class="row">
            
            <div class="col-md-3">
            <div class="box box-primary">
            <div class="box-header with-border">
            <h3 class="box-title">Latest News</h3>
            
			<div class="box-body">
            
            <table class="table table-hover">
            
            <tbody>
            
            <tr>
            <td><strong>Headline</strong></td>
            </tr>
            <tr>            
            <td>
            <p>
            <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
            </p>
            </td>           
           	<td>
            <p class="text-muted">
            sub headline
            </p>
            </td>            
            </tr>
            
            <tr>
            <td><strong>Headline</strong></td>
            </tr>
            <tr>            
            <td>
            <p>
            <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
            </p>
            </td>           
           	<td>
            <p class="text-muted">
            sub headline
            </p>
            </td>            
            </tr>
            
            <tr>
            <td><strong>Headline</strong></td>
            </tr>
            <tr>            
            <td>
            <p>
            <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
            </p>
            </td>           
           	<td>
            <p class="text-muted">
            sub headline
            </p>
            </td>            
            </tr>
            
            </tbody>
            </table>


            </div>
			</div>
            </div>
            </div>
          

     
     <div class="col-md-9">
     
     <div class="box box-primary">
     <div class="box-header with-border">
     <h3 class="box-title">All News</h3>     
     <div class="box-body">
     
     <table class="table table-hover">
            
            <tbody>
            
            <tr>
              <td colspan="2"><strong>Headline</strong></td>
            </tr>
            
            <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="150" height="200">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              subheadline--------------------------------------------------------------------------------------
              </p>
              </td>            
            </tr>
            
                        <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="150" height="200">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              subheadline--------------------------------------------------------------------------------------
              </p>
              </td>            
            </tr>
            
                        <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="150" height="200">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              subheadline--------------------------------------------------------------------------------------
              </p>
              </td>            
            </tr>
            
                        <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="150" height="200">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              subheadline--------------------------------------------------------------------------------------
              </p>
              </td>            
            </tr>
            
                        <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="150" height="200">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              subheadline--------------------------------------------------------------------------------------
              </p>
              </td>            
            </tr>
            
                        <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="150" height="200">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              subheadline--------------------------------------------------------------------------------------
              </p>
              </td>            
            </tr>
            
            <tr>
            <td colspan="2">
                  <ul class="pagination pagination-sm no-margin pull-right">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
            </td>
            </tr>
            
            </tbody>
            </table>

     
     </div>
     </div>
     </div>
     
     </div>
     
     </div>
        
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->