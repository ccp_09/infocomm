

  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
            <div class="row">
			
            <div class="col-md-9">
            <div class="box box-primary">
            <div class="box-header with-border">
            
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <a href="<?php echo base_url();?>index.php/viewProjects"><img src="<?php echo base_url() ?>dist/img/homesliderimg/placeholder.jpg" alt="First slide"></a>

                    <div class="carousel-caption">
                      First Slide
                    </div>
                  </div>
                  <div class="item">
                    <a href="<?php echo base_url();?>index.php/viewPublications"><img src="<?php echo base_url() ?>dist/img/homesliderimg/placeholder.jpg" alt="Second slide"></a>

                    <div class="carousel-caption">
                      Second Slide
                    </div>
                  </div>
                  <div class="item">
                    <a href="<?php echo base_url();?>index.php/ourteam"><img src="<?php echo base_url() ?>dist/img/homesliderimg/placeholder.jpg" alt="Third slide"></a>

                    <div class="carousel-caption">
                      Third Slide
                    </div>
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
            
			</div>
            </div>
            </div>
            
            <div class="col-md-3">
            <div class="box box-primary">
            <div class="box-header with-border">
            <h3 class="box-title">Upcoming Events</h3>
            
			<div class="box-body">
            
            <table class="table table-hover">
            
            <tbody>
            
            <tr>
            <td><strong>Event Name</strong></td>
            </tr>
            <tr>            
            <td>
            <p>
            <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
            </p>
            </td>           
           	<td>
            <p class="text-muted">
            details yada yada yada yada
            details yada yada yada yada
            </p>
            </td>            
            </tr>
            
            <tr>
            <td><strong>Event Name</strong></td>
            </tr>
            <tr>            
            <td>
            <p>
            <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
            </p>
            </td>           
           	<td>
            <p class="text-muted">
            details yada yada yada yada
            details yada yada yada yada
            </p>
            </td>            
            </tr>
            
            <tr>
            <td><strong>Event Name</strong></td>
            </tr>
            <tr>            
            <td>
            <p>
            <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
            </p>
            </td>           
           	<td>
            <p class="text-muted">
            details yada yada yada yada
            details yada yada yada yada
            </p>
            </td>            
            </tr>
            
            <tr>
            <td colspan="2">
            <a href="<?php echo base_url();?>index.php/viewProjects"><button type="button" class="btn btn-block btn-primary btn-xs">See more Events</button></a>
            </td>
            </tr>
            
            </tbody>
            </table>


            </div>
			</div>
            </div>
            </div>
          

          </div>
          <!-- end of row -->
     
     <div class="row">
     
     <div class="col-md-9">
     
     <div class="box box-primary">
     <div class="box-header with-border">
     <h3 class="box-title">Latest Projects</h3>     
     <div class="box-body">
     
     <table class="table table-hover">
            
            <tbody>
            
            <tr>
              <td colspan="2"><strong>Project Name</strong></td>

              <td colspan="2"><strong>Project Name</strong></td>

            </tr>
            
            <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>
             
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
              </p>
              </td> 
                        
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>            
            </tr>
            
            <tr>
              <td colspan="2"><strong>Project Name</strong></td>

              <td colspan="2"><strong>Project Name</strong></td>

            </tr>
            
            <tr>            
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
              </p>
              </td>           
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>
             
              <td>
              <p>
              <img src="<?php echo base_url() ?>dist/img/upcomingevents/side1.jpg" width="80" height="80">
              </p>
              </td> 
                        
              <td>
              <p class="text-muted">
              details yada yada yada yada
              details yada yada yada yada
              </p>
              </td>            
            </tr>
            
            <tr>
            <td colspan="4">
            <a href="<?php echo base_url();?>index.php/viewProjects"> <button type="button" class="btn btn-block btn-primary btn-xs">See more Projects</button></a>
            </td>
            </tr>
            
            </tbody>
            </table>

     
     </div>
     </div>
     </div>
     
     </div>
     
     </div>
        
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->