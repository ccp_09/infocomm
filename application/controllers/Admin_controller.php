<?php 
	class Admin_controller extends CI_Controller{
				
		//used to load the url to this
		function __construct() { 
			parent::__construct(); 
			$this->load->helper('url');
		}
		
		//Delete User
		function deleteUser(){
			//loads the model
			$this->load->model('Admin_model');
			
			//gets from form and assigns to the private variables
			$id = $_POST["id"];
			
			//calls the admin model and the deleteUser method in that model and passes the ID to the method
			$this->Admin_model->deleteu($id);			 
			 
			//redirects to the view
			$this->load->view('admin/viewVacancies');
		}
		
		//Edit Access Level User
		function editAccess(){
			//loads the model
			$this->load->model('Admin_model');
			
			//gets from form and assigns to the private variables
			$id = $_POST["uid"];
			$access = $_POST["accessL"];
			
			//calls the admin model and the editAccessLevel method in that model and passes the ID and new access level
			$this->Admin_model->editAccessLevel($id, $access);			 
			 
			//redirects to the view
			$this->load->view('admin/users');
		}
		
		//Delete Vacancy
		function deleteVacancy(){
			//loads the model
			$this->load->model('Admin_model');
			
			//gets from form and assigns to the private variables
			$id = $_POST["id"];
			
			//calls the admin model and the deleteVacancy method in that model and passes the ID to the method
			$this->Admin_model->deletev($id);			 
			
			//redirects to the view
			$this->load->view('admin/viewVacancies');
		}
		
		//Edit Vacancy
		function editVacancy(){
			//loads the model
			$this->load->model('Admin_model');
			
			//gets from form and assigns to the private variables
			$id = $_POST["id"];
			$title = $_POST["title"];
			$description = $_POST["description"];
			$deadline = $_POST["deadline"];			
			
			//calls the admin model and the vacancyEdit method in that model and passes the data
			$thing = $this->Admin_model->vacancyEdit($id, $title, $description, $deadline);			 
			
			echo $thing; 
			 
			//redirects to the view
			$this->load->view('admin/viewVacancies');
		}
		
		//Delete Publication
		function deletePub(){
			//loads the model
			$this->load->model('Admin_model');
			
			//gets from form and assigns to the private variables
			$id = $_POST["id"];
			
			//calls the admin model and the deletePublication method in that model and passes the ID to the method
			$this->Admin_model->deletePublication($id);			 
			
			//redirects to the view
			$this->load->view('admin/publications');
		}
		
		//Add Vacancy
		function newVacancy(){
			//loads the model
			$this->load->model('Admin_model');
			
			//gets from form and assigns to the private variables
			$title = $_POST["title"];
			$deadline = $_POST["deadline"];
			$description = $_POST["description"];
			$loginReq = $_POST["loginReq"];
			$cat = $_POST["cats"];
			
			
			//calls the admin model and the addVacancy method in that model and passes the data to the method
			$result = $this->Admin_model->addVacancy($title, $deadline, $description, $loginReq, $cat);			 
			
			if ($result)
			{
				//redirects to the view
				$this->load->view('admin/viewVacancies');
			}
		}
		
		//Delete Job Application
		function deleteApp(){
			//loads the model
			$this->load->model('Admin_model');
			
			//gets from form and assigns to the private variables
			$id = $_POST["id"];
			
			//calls the admin model and the deleteApplications method in that model and passes the ID to the method
			$this->Admin_model->deleteApplication($id);			 
			
			//redirects to the view
			$this->load->view('admin/applications');
		}
		
	}
?>