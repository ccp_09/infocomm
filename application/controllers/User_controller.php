<?php 
	class User_controller extends CI_Controller{
		
		function __construct() { 
			parent::__construct(); 
			$this->load->helper('url');
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function for user registration
		**/
		/////////////////////////////////////////////////////////////////////
		function registerUserController(){
			$this->load->model('User_model');
			$pwd = $this->security($_POST["pwd"]);
			 $data = array( 
				'title' => $_POST["title"], 
				'fname' => $_POST["fname"],
				'lname' => $_POST["lname"],
				'gender' => $_POST["gender"],
				'dob' => $_POST["DOB"],
				'email' => $_POST["email"],
				'password' => $pwd,
				'userType' => $_POST["userType"],
				'dateJoined' => "now()" ,
				'city' => $_POST["city"],
				'country' => $_POST["country"]
				
			 ); 
			
			 if($this->User_model->registerUserModel($data)){
				 echo "success";
			 }else{
				 echo "failed";
			 }
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to encrypt user password
		**/
		/////////////////////////////////////////////////////////////////////
		function security($password){
			$options = [ 'cost' => 12, ];
			$pwd = password_hash($password, PASSWORD_BCRYPT, $options);
			return $pwd;
		}
		
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to verify and login user using his email and password
		**/
		/////////////////////////////////////////////////////////////////////
		function loginUser(){
         	$this->load->library('session');
			$pwd = $_POST["pwd"];
			$email = $_POST["email"];
			$this->db->select('email, password');
			$this->db->from('users');
			$this->db->where('email', $email);
			$this->db->where('status', 'active');
				
			$query = $this->db->get();
			$output = $query->result();
			
			foreach ($output as $result)
			{
				$pwdDB = $result->password;
			}
			if(password_verify($pwd, $pwdDB)){
				$sessionData = array('email'=>$email, 'password'=>$pwdDB,'logged_in' => TRUE);
				$this->session->set_userdata($sessionData);
				$date = date('Y-m-d H:i:s');
				$this->db->set('lastLogin', $date);
				$this->db->where('email', $email);
				$this->db->update('users');
				echo "Valid";
			}else{
				echo "Invalid";
			}
			
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to update user bio and interests
		**/
		/////////////////////////////////////////////////////////////////////
		function updateUserController(){
			$this->load->model('User_model');
			$id = $_POST["id"];
			$title =  $_POST["title"]; 
			$city = $_POST["city"]; 
			$country = $_POST["country"];
			$phone = $_POST["phone"];
			$bio =  $_POST["bio"];
			$interest =  $_POST["interest"];
			if($this->User_model->updateUserModel($id,$title,$city,$country,$phone,$bio,$interest)){
			 	echo 'added';
			}else{
				echo 'Failed';
			}
			 
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to update user social media account links
		**/
		/////////////////////////////////////////////////////////////////////
		function updateUserSocialController(){
			$this->load->model('User_model');
			$id = $_POST["id"];
			$facebook =  $_POST["facebook"]; 
			$googleplus = $_POST["googleplus"]; 
			$linkedIn = $_POST["linkedIn"];
			$twitter = $_POST["twitter"];
			$website =  $_POST["website"];
			if($this->User_model->updateUserSocialModel($id,$facebook,$googleplus,$linkedIn,$twitter,$website)){
			 	echo 'added';
			}else{
				echo 'Failed';
			}
			 
		}
		
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to update user password
		**/
		/////////////////////////////////////////////////////////////////////
		function updateUserSecurityController(){
			$this->load->model('User_model');
			$id = $_POST["id"];
			$oldPassword = $_POST["oldPassword"]; 
			$newPassword = $this->security($_POST["newPassword"]);
			
			$this->db->select('password');
			$this->db->from('users');
			$this->db->where('userID', $id);
				
			$query = $this->db->get();
			$output = $query->result();
			
			foreach ($output as $result)
			{
				$pwdDB = $result->password;
			}
			if(password_verify($oldPassword, $pwdDB)){
				if($this->User_model->updateUserSecurityModel($id,$newPassword)){
					echo 'Password Updated';
				}else{
					echo 'Password failed to Update';
				}
			}else{
				echo 'Wrong Password';
			}
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Add User Qualification 
		**/
		/////////////////////////////////////////////////////////////////////
		
		function addUserQualification(){
			$this->load->model('User_model');
			$UserID = $_POST["id"];
			$value = $_POST["value"];
			$column = $_POST["column"];
			$data = array('userID' => $UserID); 
			if($value == '' && !empty($data)){
				$this->User_model->addUserEducationModel('', '', '', $data);
				echo 'Added';
			}else{
				$this->User_model->addUserEducationModel($UserID, $value, $column, '');
				echo 'Education Update Successfully';
			}
			
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to disable account if requested
		**/
		/////////////////////////////////////////////////////////////////////
		function deleteUserAccount(){
			$id = $_POST["id"];
			$this->db->set('status', 'inactive');
			$this->db->where('userID', $id);
			$query = $this->db->update('users');
			
			if($query){
				echo "done";
			}
			
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to logout and destroy user session
		**/
		/////////////////////////////////////////////////////////////////////
		function logout(){
			$this->load->library('session');
			unset($_SESSION['email'],$_SESSION['password'],$_SESSION['logged_in']);
			session_destroy();
			header("location: http://localhost/infocomm/user/login");
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to retrieve user information after logging in
		**/
		/////////////////////////////////////////////////////////////////////
		function getUpdatedProfile(){
			$eduOutput = '';
			
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('userID', $_POST["id"]);
			$this->db->where('status', 'active');
			
			$query = $this->db->get();
			$output = $query->result();
			foreach($output as $result){
				$id = $result->userID;
				$titlee = $result->title;
				$fname = $result->fname;
				$lname = $result->lname;
				$designation = $result->designation;
				$city = $result->city;
				$country = $result->country;
				$phone = $result->phone;
				$bio = $result->bio;
				$interests = $result->interest;
				$facebook = $result->facebook;
				$googleplus = $result->googlePlus;
				$linkedIn = $result->linkedIn;
				$twitter = $result->twitter;
				$website = $result->website;
			}
			
			$this->db->select('*');
			$this->db->from('user_education');
			$this->db->where('userID', $id);
			$query = $this->db->get();
			$output = $query->result();
			foreach($output as $userEducation){
				$EduID = $userEducation->id;
				$institue = $userEducation->institute;
				$qualification = $userEducation->qualification;
				$startDate = $userEducation->startDate;
				$gradDate = $userEducation->gradDate;
				
				$eduOutput .= '<tr>
						<td>Institute</td><td id="institue" data-id1="'.$EduID.'" contenteditable="true">'.$institue.'</td>
					</tr>
					<tr>
						<td>Qualification</td><td id="qualif" data-id2="'.$EduID.'" contenteditable="true">'.$qualification.'</td>
					</tr>
					<tr>
						<td>Start Date</td><td id="startDate" data-id3="'.$EduID.'" contenteditable="true">'.$startDate.'</td>
					</tr>
					<tr>
						<td>Graduation Date</td><td id="endDate" data-id4="'.$EduID.'" contenteditable="true">'.$gradDate.'</td>
					</tr>
					<tr><td colspan="2"><button id="deleteEdu" style="float:right;" class="btn btn-danger">Delete Education</button></td></tr>';
			}
			
			$editModalOutput ='
			<div class="modal-header">
			  <button id="closeModalBtn" style="float:right;" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  <h4 class="modal-title">Edit Profile - '.$titlee.' '.$fname.' '.$lname.'</h4>
			</div>
			<div class="modal-body"><span id="modalStatus"></span>
			  <p>
				<h2>Bio and Interest</h2>
				<table id="updateProfileTable" align="center" class="table table-responsive table-bordered">
					<tr>
						<td style="width:150px;">Title</td><td><select class="form-control" id="title">
						<option>'.$titlee.'</option>
						<option>Dr.</option>
						<option>Prof.</option>
						<option>Mr.</option>
						<option>Mrs.</option>
						<option>Ms.</option>
					</select></td>
					</tr>
					<tr>
						<td>City</td><td id="city" contenteditable="true">'.$city.'</td>
					</tr>
					<tr>
						<td>Country</td><td id="country" contenteditable="true">'.$country.'</td>
					</tr>
					<tr>
						<td>Phone</td><td id="phone" contenteditable="true">'.$phone.'</td>
					</tr>
					<tr>
						<td>Bio</td><td id="bio" style="height:150px;" contenteditable="true">'.$bio.'</td>
					</tr>
					<tr>
						<td>Interest</td><td id="interest" contenteditable="true">'.$interests.'</td>
					</tr>
					<tr><td colspan="2"><button id="btnBioUpdate" style="float:right;" class="btn btn-success">Update Bio</button></td></tr>
				</table>
				
				<h2>Education</h2>
				<table id="eduTable" align="center" class="table table-responsive table-bordered">
					'.$eduOutput.'
					<tr><td colspan="2"><button id="addEducationBTN" style="float:left;" class="btn btn-warning">Add Education</button></td></tr>
				</table>
				
				<h2>Social Media and Wesite</h2>
				<table align="center" class="table table-responsive table-bordered">
					<tr>
						<td style="width:150px;">Facebook</td><td id="facebook" contenteditable="true">'.$facebook.'</td>
					</tr>
					<tr>
						<td>Google+</td><td id="googleP" contenteditable="true">'.$googleplus.'</td>
					</tr>
					<tr>
						<td>LinkedIn</td><td id="linkedIn" contenteditable="true">'.$linkedIn.'</td>
					</tr>
					<tr>
						<td>Twitter</td><td id="twitter" contenteditable="true">'.$twitter.'</td>
					</tr>
					<tr>
						<td>Website</td><td id="website" contenteditable="true">'.$website.'</td>
					</tr>
					<tr><td colspan="2"><button id="btnSocialUpdate" style="float:right;" class="btn btn-success">Update Social</button></td></tr>
				</table>
				
				<h2>Security</h2>
				<table align="center" class="table table-responsive table-bordered">
					<tr>
						<td style="width:150px;">Current Password</td><td><input id="oldPassword" type="password" class="form-control" /></td>
					</tr>
					<tr>
						<td>New Password</td><td><input id="newPassword" type="password" class="form-control" /></td>
					</tr>
					<tr>
						<td>Confirm Password</td><td><input id="matchPassword" type="password" class="form-control" /></td>
					</tr>
					<tr><td colspan="2"><button id="btnSecurityUpdate" style="float:right;" class="btn btn-success">Update Password</button></td></tr>
				</table>
				 </p>
			</div>
			<div class="modal-footer">
			<button id="disableAccount" style="float:left;" type="button" class="btn btn-danger">Delete Account</button>
		  </div>';
				
				echo $editModalOutput;
		}
	}
?>