<?php
	class Admin_model extends CI_Model{
	
		function __construct() { 
			parent::__construct(); 
		} 
		
		//Delete User
		public function deleteu($ID){
			$myQuery = $this->db->delete("users", "userID = ".$ID);
			if($myQuery){
				return true;
			}else{
				return false;
			}
		}
		
		//Edit Access Level
		public function editAccessLevel($ID, $access){
			$this->db->set('accessLevel', $access);
			$this->db->where('userID', $ID);
			$myQuery = $this->db->update('users');
			 
			if($myQuery){
				return true;
			}else{
				return false;
			}
		}
		
		//Delete Vacancy
		public function deletev($ID){
			$myQuery = $this->db->delete("vacancies", "vID = ".$ID);
			if($myQuery){
				return true;
			}else{
				return false;
			}
		}
		
		//Delete Job Application
		public function deleteApplication($id){
			$myQuery = $this->db->delete("v_applications", "appID = ".$id);
			if($myQuery){
				return true;
			}else{
				return false;
			}
		}
		
		//Edit Vacancy
		public function vacancyEdit($id, $title, $description, $deadline){
			
			//sets the new information
			$this->db->set('title', $title);
			$this->db->set('description', $description);
			$this->db->set('deadline', $deadline);
			//where the id is equal to the id passed to the method
			$this->db->where('vID', $id);
			//in the vacancies table
			$myQuery = $this->db->update('vacancies');
			
			//returns status to the view via the controller
			if($myQuery){
				return true;
			}else{
				return false;
			}
		}
		
		
		//Delete Vacancy
		public function deletePublication($ID){
			$myQuery = $this->db->delete("user_publications", "pubID = ".$ID);
			if($myQuery){
				return true;
			}else{
				return false;
			}
		}
		
		
		
		//Add Vacancy
		public function addVacancy($title, $deadline, $description, $loginReq, $cat){
			
			$data = array( 
				'title' => $title,
				'deadline' => $deadline,
				'description' => $description,
				'loginRequired' => $loginReq,
				'vCat' => $cat 
			 ); 
			
			$myQuery = $this->db->insert("vacancies", $data);
			
			if($myQuery){
				return true;
			}else{
				return false;
			}
		}
		
	}

?>