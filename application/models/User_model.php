<?php
	class User_model extends CI_Model{
	
		function __construct() { 
			parent::__construct(); 
		} 
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function for user registration
		**/
		//////////////////////////////////////////////////////////////////////
		public function registerUserModel($data){
			$query = $this->db->insert("users", $data);
			if($query){
				return true;
			}else{
				return false;
			}
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to update user bio and interests
		**/
		/////////////////////////////////////////////////////////////////////
		public function updateUserModel($id, $title,$city,$country,$phone,$bio,$interest){
			$this->db->set('title', $title);
			$this->db->set('city', $city);
			$this->db->set('country', $country);
			$this->db->set('phone', $phone);
			$this->db->set('bio', $bio);
			$this->db->set('interest', $interest);
			$this->db->where('userID', $id);
			$query = $this->db->update('users');
			if($query){
				return true;
			}else{
				return false;
			}
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to update user social media account links
		**/
		/////////////////////////////////////////////////////////////////////
		public function updateUserSocialModel($id,$facebook,$googleplus,$linkedIn,$twitter,$website){
			$this->db->set('facebook', $facebook);
			$this->db->set('googlePlus', $googleplus);
			$this->db->set('linkedIn', $linkedIn);
			$this->db->set('twitter', $twitter);
			$this->db->set('website', $website);
			$this->db->where('userID', $id);
			$query = $this->db->update('users');
			if($query){
				return true;
			}else{
				return false;
			}
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to Add User Education
		**/
		/////////////////////////////////////////////////////////////////////
		
		function addUserEducationModel($id, $value, $column, $data){
			if($value == '' && !empty($data)){
				$query = $this->db->insert("user_education", $data);
			}else{
				$this->db->set($column, $value);
				$this->db->where('id', $id);
				$query = $this->db->update('user_education');
			}
			if($query){
				return true;
			}else{
				return false;
			}
		}
		
		/////////////////////////////////////////////////////////////////////
		/**
		*** Function to update user password
		**/
		/////////////////////////////////////////////////////////////////////
		public function updateUserSecurityModel($id,$newPassword){
			$this->db->set('password', $newPassword);
			$this->db->where('userID', $id);
			$query = $this->db->update('users');
			if($query){
				return true;
			}else{
				return false;
			}
		}
	}

?>