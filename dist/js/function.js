////////////////////////////////////////////////////////////////////////////////
/**
*** this function get user information from database into
*** the modal when the page is loaded
*** this function also refresh the modal after each updates
**/
///////////////////////////////////////////////////////////////////////////////
function getUpdates(){
	$.post("profile/getupdatedprofile",{id:$('.modal-content').data("id")}, function(data,status){
		//alert(status);
		$('#getUpdate').html(data);
	});
}

/////////////////////////////////////////////////////////////////////
/**
*** Onlick button function that update users biography and Interests
**/
//////////////////////////////////////////////////////////////////////
$(document).on('click','#btnBioUpdate', function(){
	var id = $('.modal-content').data("id");
	var title = $('#title option:selected').text();
	var city = $('#city').text();
	var country = $('#country').text();
	var phone = $('#phone').text();
	var bio = $('#bio').text();
	var interest = $('#interest').text();
	$.post("profile/updatebio", {id:id,title:title,city:city,country:country,phone:phone,bio:bio,interest:interest}, function(data, status){
		if(data == 'added'){
			$('#modalStatus').css('color', 'green');
			$('#modalStatus').html('<strong>Updated Successfully</strong>')
		}else{
			$('#modalStatus').css('color', 'red');
			$('#modalStatus').html('<strong>Error Updating</strong>')
		}
		getUpdates();
	})
})

/////////////////////////////////////////////////////////////////////
/**
*** Onlick button function that update user's social media links
**/
/////////////////////////////////////////////////////////////////////

$(document).on('click','#btnSocialUpdate', function(){
	var id = $('.modal-content').data("id");
	var facebook = $('#facebook').text();
	var googleplus = $('#googleP').text();
	var linkedIn = $('#linkedIn').text();
	var twitter = $('#twitter').text();
	var website = $('#website').text();
	
	$.post("profile/updatesocial", {id:id,facebook:facebook,googleplus:googleplus,linkedIn:linkedIn,twitter:twitter,website:website}, function(data, status){
		if(data == 'added'){
			$('#modalStatus').css('color', 'green');
			$('#modalStatus').html('<strong>Updated Successfully</strong>')
		}else{
			$('#modalStatus').css('color', 'red');
			$('#modalStatus').html('<strong>Error Updating</strong>')
		}
		getUpdates();
	})
})

/////////////////////////////////////////////////////////////////////
/**
*** Onlick button function that updates user's Password
**/
/////////////////////////////////////////////////////////////////////
$(document).on('click','#btnSecurityUpdate', function(){
	var id = $('.modal-content').data("id");
	var oldPassword = $('#oldPassword').val();
	var newPassword = $('#newPassword').val();
	var passwordToMatch = $('#matchPassword').val();
	if(validatePassword()&&matchPassword()&& oldPassword !== ''){
		$.post("profile/updatesecurity", {id:id,oldPassword:oldPassword,newPassword:newPassword}, function(data, status){
			alert(data)
			getUpdates();
		})
	}else{
		alert("Error")
	}
})

/////////////////////////////////////////////////////////////////////
/**
*** Password Validation function
**/
/////////////////////////////////////////////////////////////////////
function validatePassword(){
	var newPassword = $('#newPassword').val();
	var checkstrength = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
	if(newPassword == ''){
		alert("Password must not be empty");
	}else if(newPassword.length < 7){
		alert("Password must be greater than 7 characters");
		return false;	
	}else if(!newPassword.match(checkstrength)){
		alert("password must contain at least one uppercase, lowercase and number");
		return false;
	}else{
		return true;
	}	
}
function matchPassword() {
	var newPassword = $('#newPassword').val();
	var passwordToMatch = $('#matchPassword').val();
	if(passwordToMatch.match(newPassword)){
		return true;
	}else{
		alert("Password don't match");
		return false;
	}
}

/////////////////////////////////////////////////////////////////////
/**
*** Function to delete User account
**/
/////////////////////////////////////////////////////////////////////
$(document).on('click','#disableAccount', function(){
	if(confirm("Are you sure you want to disable your account? ")){
		$.post("profile/disable",{id:$('.modal-content').data("id")}, function(data,status){
			if(data == 'done'){
				window.location = "http://localhost/infocomm/logout/logout";
			}
		});
	}
})

/////////////////////////////////////////////////////////////////////
/**
*** This function post user data to update when the text fields 
*** looses focus
**/
/////////////////////////////////////////////////////////////////////

function addEducation(id, value, column){
	$.post('profile/addEdu',{id:id, value:value, column:column}, function(data, success){
	})
}

/////////////////////////////////////////////////////////////////////
/**
*** Page ready function load other function when the page loaded
**/
/////////////////////////////////////////////////////////////////////
$(document).ready(function() {
	getUpdates();
});

/////////////////////////////////////////////////////////////////////
/**
*** This function redirects to either login or registration page
*** when the button is clicked
**/
/////////////////////////////////////////////////////////////////////
$(document).ready(function() {
	$('#loginLink').on('click', function(){
		window.location.href = "http://localhost/infocomm/user/login";
	})
	$('#registerLink').on('click', function(){
		window.location.href = "http://localhost/infocomm/user/register";
	})
	
	$(document).on('click','#addEducationBTN', function(){
	$('#eduTable').append('<tr><td>Institute</td><td id="institue" contenteditable="true"></td></tr>\
				<tr><td>Qualification</td><td id="qualif" contenteditable="true"></td></tr>\
				<tr><td>Start Date</td><td id="startDate" contenteditable="true"></td></tr>\
				<tr><td>Graduation Date</td><td id="endDate" contenteditable="true"></td></tr>\
				<tr><td colspan="2"><button id="deleteEdu" data-id="" style="float:right;" class="btn btn-danger">Delete Education</button></td></tr>');
				addEducation($('.modal-content').data("id"), '', '')
	})
	
	$(document).on('blur','#institue',function(){
		var id = $(this).data("id1");
		var text = $(this).text();
		addEducation(id, text, 'institute')
		getUpdates();
	})
	$(document).on('blur','#qualif',function(){
		var id = $(this).data("id2");
		var text = $(this).text();
		addEducation(id, text, 'qualification')
		getUpdates();
	})
	
	$(document).on('click', '#closeModalBtn', function(){
		location.reload();
	})

});